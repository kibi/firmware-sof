firmware-sof (2.2.3-2) UNRELEASED; urgency=medium

  * Move source and binary from non-free/kernel to non-free-firmware/kernel
    following the 2022 General Resolution about non-free firmware.
  * Adjust disclaimer in the copyright file as well.

 -- Cyril Brulebois <kibi@debian.org>  Tue, 17 Jan 2023 19:57:53 +0000

firmware-sof (2.2.3-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable):
    + firmware-sof-signed: Drop versioned constraint on dpkg in Pre-Depends.

  [ Mark Pearson ]
  * Update to upstream version 2.2.3 

 -- Mark Pearson <markpearson@lenovo.com>  Fri, 09 Dec 2022 15:55:57 -0500

firmware-sof (2.2.2-1) unstable; urgency=medium

  * Update to upstream version 2.2.2 - Using tarball release.
  * Use dpkg-maintscript-helper to convert sof-tplg from symlink to dir.

 -- Mark Pearson <markpearson@lenovo.com>  Mon, 03 Oct 2022 20:03:19 -0400

firmware-sof (2.1.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Update standards version to 4.6.0, no changes needed.

  [ Mark Pearson ]
  * Update to upstream version 2.1.1

 -- Mark Pearson <markpearson@lenovo.com>  Sun, 24 Apr 2022 22:04:04 -0400

firmware-sof (2.0-1) unstable; urgency=medium

  * Update to upstream version 2.0

 -- Mark Pearson <markpearson@lenovo.com>  Fri, 21 Jan 2022 22:35:39 -0500

firmware-sof (1.9-1) unstable; urgency=medium

  * Update to upstream version 1.9

 -- Mark Pearson <markpearson@lenovo.com>  Mon, 04 Oct 2021 22:35:20 -0400

firmware-sof (1.7-1) unstable; urgency=medium

  * Update to upstream version 1.7

 -- Mark Pearson <markpearson@lenovo.com>  Sun, 13 Jun 2021 14:05:46 -0400

firmware-sof (1.6.1-2) unstable; urgency=medium

  * Remove postinst file as we don't need to add files to initramfs
  * Add XS-Autobuild configuration to control file

 -- Mark Pearson <markpearson@lenovo.com>  Mon, 18 Jan 2021 13:13:10 -0500

firmware-sof (1.6.1-1) unstable; urgency=medium

  * Update to upstream version 1.6.1

 -- Mark Pearson <markpearson@lenovo.com>  Mon, 04 Jan 2021 21:03:12 -0500

firmware-sof (1.6~rc3-1) unstable; urgency=medium

  * Initial release. (Closes: #960788, #962134)

 -- Mark Pearson <markpearson@lenovo.com>  Sun, 13 Dec 2020 18:02:46 -0500
